﻿using System;
using System.Drawing;
using System.Linq;

namespace GeometryFigures
{
    /// <summary>
    /// Основной класс.
    /// </summary>
    internal class Program
    {
        #region Методы
               
        /// <summary>
        /// Основной метод.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            while (true) 
            { 
                Console.WriteLine("Введите количество точек: (3,4,6)");            
                int.TryParse(Console.ReadLine(),out int input);
                if (input == 3 || input == 4 || input ==6)
                {
                    Console.WriteLine("Введите координаты точек в формате: X Y");
                    PointF[] figurePoints = new PointF[input];
                    Random rnd = new Random();
                    for (int i = 0; i < input; i++)
                    {
                        //Ручной ввод
#if !DEBUG
                        Console.Write($"Точка {i+1}: ");
                        var tempVar = Console.ReadLine();
                        var points = tempVar.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (points.Count() != 2)
                        {
                            input = -1;
                            break;
                        }
                        figurePoints[i] = new PointF(float.Parse(points[0]), float.Parse(points[1]));
#endif

                        //Рандомный ввод на интервале [a,b]
#if DEBUG
                        int a = 0;   // 0
                        int b = 100;
                        float x = (float) rnd.NextDouble()*(b - a) + a;
                        float y = (float) rnd.NextDouble()*(b - a) + a;
                        figurePoints[i] = new PointF(x,y);
#endif

                    }

                    if (input == 3)
                    {
                        Triangle treengle = new Triangle(input, figurePoints);
                        if(treengle.isPossibleFigure)
                        {
                            Console.WriteLine("Фигура возможна.");
                            treengle.CalculateParametrsFigure(treengle.countSides, treengle.coordinatesFigure);
                            treengle.PrintDataFigyre();
                        }
                        else
                        {
                            Console.WriteLine("Фигура не возможна.");
                        }                        
                    }

                    else if (input == 4)
                    {
                        Quadrilateral quadrilateral = new Quadrilateral(input, figurePoints);
                        if (quadrilateral.isPossibleFigure)
                        {
                            Console.WriteLine("Фигура возможна.");
                            quadrilateral.GetTypeQuadrilateral();
                            if (quadrilateral.typeQuadrilateral ==  "НЕ простой")
                            {
                                quadrilateral.square = -1;
                                quadrilateral.perimetr = -1;
                                Console.WriteLine($"Данный четрыехугольник - {quadrilateral.typeQuadrilateral}.");
                            }
                            else
                            {
                                Console.WriteLine($"Данный четрыехугольник  {quadrilateral.typeQuadrilateral}.");
                                quadrilateral.CalculateParametrsFigure(quadrilateral.countSides, quadrilateral.coordinatesFigure);
                                quadrilateral.PrintDataFigyre();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Фигура не возможна.");
                        }                                              
                        
                    }
                    else if (input == 6)
                    {
                        Hexagon hexagon1 = new Hexagon(input, figurePoints);

                        if (hexagon1.isPossibleFigure)
                        {
                            Console.WriteLine("Фигура возможна.");
                            if (hexagon1.angles.Sum() == 180*(hexagon1.countSides-2))
                            {
                                Console.WriteLine("Шестиугольник - Простой");
                                hexagon1.CalculateParametrsFigure(hexagon1.countSides, hexagon1.coordinatesFigure);
                                hexagon1.PrintDataFigyre();
                            }
                            else
                            {
                                Console.WriteLine("Шестиугольник НЕ простой.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Фигура не возможна.");
                        }
                    }                    
                    Console.WriteLine("");
                }
                else { Console.WriteLine("Неверный ввод."); }                    
            }
        }

        #endregion
    }
}
