﻿using System;
using System.Drawing;
using System.Linq;

namespace GeometryFigures
{
    /// <summary>
    /// Класс четырехугольников.
    /// </summary>
    public class Quadrilateral : BaseFigure
    {
        #region Поля и свойства

        /// <summary>
        /// Тип четырехугольника.
        /// </summary>
        public string typeQuadrilateral { get; set; }

        #endregion

        #region Методы

        /// <summary>
        /// Определение типа четырехугольника.
        /// </summary>
        public void GetTypeQuadrilateral()
        {
            if ((this.sides[0] == this.sides[1] && this.sides[1] == this.sides[2]) && (this.angles[0] == 90 &&  this.angles[1] == 90 && this.angles[2] == 90))
            {
                typeQuadrilateral = "Квадрат";
            }
            else if (this.angles[0] == 90 &&  this.angles[1] == 90 && this.angles[2] == 90)
            {
                typeQuadrilateral = "Прямоугольник";
            }
            else if ((int)Math.Round(this.angles.Sum()) == (int)(180*(this.countSides-2)))
            {
                typeQuadrilateral = "Простой";
            }
            else
            {
                typeQuadrilateral = "НЕ простой";
            }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Расчет параметров четырехугольника.
        /// </summary>
        /// <param name="countSides">Количество сторон.</param>
        /// <param name="pointsFigure">Массив координат.</param>
        public Quadrilateral(int countSides, PointF[] pointsFigure) : base(countSides, pointsFigure)
        {
        }

        #endregion
    }
}
