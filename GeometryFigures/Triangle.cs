﻿using System.Drawing;

namespace GeometryFigures
{
    /// <summary>
    /// Класс треугольников.
    /// </summary>
    public class Triangle : BaseFigure
    {
        #region Конструкторы

        /// <summary>
        /// Расчет параметров треугольника.
        /// </summary>
        /// <param name="countSides">Количество сторон.</param>
        /// <param name="pointsFigure">Массив координат.</param>
        public Triangle(int countSides, PointF[] pointsFigure) : base(countSides, pointsFigure)
        {
        }

        #endregion
    }
}
