﻿using System.Drawing;

namespace GeometryFigures
{
    /// <summary>
    /// Класс шестиугольников.
    /// </summary>
    public class Hexagon : BaseFigure
    {
        #region Конструкторы
        /// <summary>
        /// Конструктор шестиугольника.
        /// </summary>
        /// <param name="countSides">Количество сторон.</param>
        /// <param name="pointsFigure">Массив координат.</param>
        public Hexagon(int countSides, PointF[] pointsFigure) : base(countSides, pointsFigure)
        {
        }

        #endregion
    }
}
