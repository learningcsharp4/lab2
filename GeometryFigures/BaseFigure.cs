﻿using System;
using System.Drawing;
using System.Linq;


namespace GeometryFigures
{
    public class BaseFigure
    {
        #region Поля и свойства

        /// <summary>
        /// Количество сторон фигуры.
        /// </summary>    
        public int countSides;
        /// <summary>
        /// Координаты фигуры.
        /// </summary>
        public PointF[] coordinatesFigure;
        /// <summary>
        /// Массив сторон фигуры.
        /// </summary>
        public double[] sides;
        /// <summary>
        /// Массив углов фигуры.
        /// </summary>
        public double[] angles;
        /// <summary>
        /// Периметр фигуры.
        /// </summary>
        public double perimetr { get; set; }
        /// <summary>
        /// Площадь фигуры.
        /// </summary>
        public double square { get; set; }
        /// <summary>
        /// Фигура возможна?
        /// </summary>
        public bool isPossibleFigure { get; set; }

        #endregion

        #region Методы

        /// <summary>
        /// Получение длин сторон фигуры.
        /// </summary>
        /// <param name="countSides">Количество сторон фигуры.</param>
        /// <param name="inputPoints">Массив точек фигуры PointF.</param>
        /// <returns>Массив длин сторон фигуры.</returns>
        private double[] GetSides(int countSides, PointF[] inputPoints)
        {
            sides = new double[countSides];
            for (int i = 0; i < countSides; i++)
            {
                sides[i] = GetLengthBetweenTwoPoints(inputPoints[(i%countSides)], inputPoints[(i+1)%countSides]);
            }
            return sides;
        }

        /// <summary>
        /// Расчет расстояния между 2 точками.
        /// </summary>
        /// <param name="point1">Координаты точки 1.</param>
        /// <param name="point2">Координаты точки 2.</param>
        /// <returns>Длина между 2 точками.</returns>
        private double GetLengthBetweenTwoPoints(PointF point1, PointF point2)
        {
            return Math.Sqrt((point1.X-point2.X)*(point1.X-point2.X)+(point1.Y-point2.Y)*(point1.Y-point2.Y));
        }

        /// <summary>
        /// Проверка на возможность фигуры.
        /// </summary>
        /// <param name="pointsFigure">Точки фигуры.</param>
        /// <returns>Да(возможен)/Нет</returns>
        public bool IsPossibleFigure(PointF[] pointsFigure)
        {
            // firstTry лежит ли 3 точка на прямой с 1 и 2 (True - Лежит, False - не лежит)
            var firstCheck = false;
            for (int i = 0; i < (pointsFigure.Length-2); i++)
            {
                firstCheck = firstCheck || (pointsFigure[2+i].X - pointsFigure[0+i].X) / (pointsFigure[1+i].X - pointsFigure[0+i].X) ==
                 (pointsFigure[2+i].Y - pointsFigure[0+i].Y) / (pointsFigure[1+i].Y - pointsFigure[0+i].Y);
            }
            // Перебор всех комбинаций точек
            var secondCheck = false;
            for (int i = 0; i < pointsFigure.Length; i++)
            {
                for (int j = 0; j < pointsFigure.Length; j++)
                {
                    if (i!=j)
                    {
                        secondCheck = secondCheck || Equals(pointsFigure[i], pointsFigure[j]);
                    }
                }
            }
            var result = !(firstCheck || secondCheck);
            return result;
        }

        /// <summary>
        /// Вычисление периметра фигуры.
        /// </summary>
        /// <param name="sidesTriangle">Массив сторон.</param>
        /// <returns>Периметр фигуры.</returns>
        private double GetPerimetr(double[] sidesTriangle)
        {
            foreach (var side in sidesTriangle)
            {
                perimetr += side;
            }
            return perimetr;
        }

        /// <summary>
        /// Получение углов треугольнька.
        /// </summary>
        /// <param name="sidesTriangle">Массив длин сторон треугольника.</param>
        /// <returns>Массив углов.</returns>
        public virtual double[] GetAngleFigure(int countSides, double[] sidesTriangle)
        {
            for (int i = 0; i < countSides; i++)
            {
                var oppositeSideOfAngle = GetLengthBetweenTwoPoints(coordinatesFigure[(i+1)%countSides], coordinatesFigure[(i+countSides-1)%countSides]);
                angles[i] = TheoremOfCosinus(sidesTriangle[i%countSides], sidesTriangle[(i+countSides-1)%countSides], oppositeSideOfAngle);
                angles[i] = Math.Acos(angles[i])*180/Math.PI;
                angles[i] = Math.Round(angles[i],6);
            }
            return angles;
        }

        /// <summary>
        /// Теорема косинусов.
        /// </summary>
        /// <param name="A">Длина прилежащей стороны 1 к вершине искомого угла.</param>
        /// <param name="B">Длина прилежащей стороны 2 к вершине искомого угла.</param>
        /// <param name="C">Длина противолежащей стороны вершины.</param>
        /// <returns> Косинус угол между прилежащими сторонами треугольника.</returns>
        private double TheoremOfCosinus(double A, double B, double C)
        {
            var result = (A*A+B*B-C*C)/(2*A*B);
            return result;
        }

        /// <summary>
        /// Вычисление площади многоугольника точки которого соеденены последовательно (Гаус).
        /// </summary>
        /// <param name="countSides">Количество сторон многоугольника.</param>
        /// <param name="inputPoints">Координаты многоугольника.</param>
        /// <returns>Площадь многоугольника.</returns>
        private double GetSquareGause(int countSides, PointF[] inputPoints)
        {
            double sum = 0;
            double dec = 0;
            for (int i = 0; i < countSides; i++)
            {
                sum += inputPoints[i%countSides].X * inputPoints[(i+1)%countSides].Y;
                dec -= inputPoints[(i+1)%countSides].X * inputPoints[Math.Abs((i)%countSides)].Y;
            }
            return square = Math.Abs(sum+dec)/2;
        }

        /// <summary>
        /// Вывод информации о фигуре в консоль.
        /// </summary>
        public void PrintDataFigyre()
        {
            Console.WriteLine("_____________Данные о фигуре______________");
            Console.WriteLine("Координаты фигуры:");
            foreach (var dots in coordinatesFigure)
            {
                Console.WriteLine(dots);
            }
            Console.WriteLine("Стороны фигуры:");
            foreach (var side in sides)
            {
                Console.WriteLine(side);
            }
            Console.WriteLine("Углы фигуры:");
            foreach (var angle in angles)
            {
                Console.WriteLine(angle);
            }
            Console.WriteLine($"Сумма углов фигуры: {angles.Sum()}");            
            Console.WriteLine($"Периметр фигуры:{perimetr}");
            Console.WriteLine($"Площадь фигуры:{square}");
            Console.WriteLine("_________________________________________");
        }

        /// <summary>
        /// Сортировка точек для образования выпоклого многоугольника.
        /// </summary>
        /// <param name="countSides">Число сторон.</param>
        /// <param name="pointsFigure">Массив точек.</param>
        public void SortCoordinates(int countSides, PointF[] pointsFigure)
        {
            Console.WriteLine("До сортировки");
            foreach (var dots in pointsFigure)
            {
                Console.WriteLine(dots);
            }
            double[] lenhtg1 = new double[countSides];
            PointF[] points = new PointF[countSides];
            points = pointsFigure;
            for (int i = 0;i < countSides;i++)
            {
                lenhtg1[i] = GetLengthBetweenTwoPoints(pointsFigure[1], pointsFigure[i]);
            }
            // SORT UP
            double temp1;
            PointF temp2;
            for (int i = 0; i < countSides; i++)
            {
                for (int j = 0; j < countSides - 1; j++)
                {
                    if (lenhtg1[j] > lenhtg1[j + 1])
                    {
                        temp1 = lenhtg1[j + 1];
                        lenhtg1[j + 1] = lenhtg1[j];
                        lenhtg1[j]=temp1;
                        temp2 = points[j+1];
                        points[j+1] = points[j];
                        points[j]=temp2;
                    }
                }
            }
            coordinatesFigure = points;
            Console.WriteLine("После сортировки");
            foreach (var dots in coordinatesFigure)
            {
                Console.WriteLine(dots);
            }
        }

        /// <summary>
        /// Сортировка точек для образования выпоклого многоугольника.
        /// </summary>
        /// <param name="countSides">Число сторон.</param>
        /// <param name="pointsFigure">Массив точек.</param>
        public void SortCoordinates2(int countSides, PointF[] pointsFigure)
        {
            int ind = 0;
            var sortingArray = new PointF[countSides];
            var oldArr = pointsFigure;
            //Крайняя левая точка
            for (int i = 0; i < countSides-1;i++)
            {
                if ((pointsFigure[i].X < pointsFigure[i+1].X))
                {
                    sortingArray[0] = pointsFigure[i];
                    ind = i;
                }
            }            
            //Вниз и вправо
            Console.WriteLine(sortingArray[0]);
            Console.WriteLine(ind);
        }

        /// <summary>
        /// Вычисление параметров фигуры.
        /// </summary>
        /// <param name="countSides">Число сторон фигуры.</param>
        /// <param name="pointsFigure">Коордиаты фигуры.</param>
        public void CalculateParametrsFigure(int countSides, PointF[] pointsFigure)
        {
            this.sides = GetSides(countSides, pointsFigure);
            this.angles = GetAngleFigure(countSides, this.sides);
            this.perimetr = GetPerimetr(this.sides);
            this.square = GetSquareGause(countSides, pointsFigure);
        }
        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор выполняющий инициализацию фигуры, если она возможена.
        /// </summary>
        /// <param name="countSides">Количество сторон.</param>
        /// <param name="pointsFigure">Координаты.</param>
        public BaseFigure(int countSides, PointF[] pointsFigure)
        {
            this.coordinatesFigure = pointsFigure;
            this.countSides = countSides;
            if (IsPossibleFigure(pointsFigure))
            {
                this.isPossibleFigure = true;
                this.sides = new double[countSides];
                this.angles = new double[countSides];
            }
            else
            {
                this.isPossibleFigure =false;
            }        
        }

        #endregion
    }
}
